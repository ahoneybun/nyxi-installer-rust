use std::io;
use std::process::Command;

fn main() {

    let mut drive_name = String::new();

    Command::new("lsblk")
    .spawn()
    .expect("lsblk command failed to start");

    let mut child = Command::new("sleep")
                            .arg("1")
                            .spawn()
                            .unwrap();

    let _result = child.wait().unwrap();

    println!("What is your drive?");

    io::stdin()
       .read_line(&mut drive_name)
       .expect("Failed to read line");

    sudo::escalate_if_needed()
         .expect("Failed to reach sudo");

    Command::new("fdisk")
            .arg(drive_name.clone())
            .arg("-w")
            .arg("always")
            .arg("-W")
            .arg("always")
            .spawn()
            .expect("Failed to write to disk");

    println!("Your drive is: {drive_name}");

}
